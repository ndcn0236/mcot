import pkg_resources
import os
from file_tree.parse_tree import available_subtrees
from pathlib import Path


def load():
    """
    Entry point for file-tree to load the mcot_pipe_* trees
    """
    directory = pkg_resources.resource_filename(__name__, "trees")
    for filename in os.listdir(directory):
        available_subtrees['mcot_pipe_' + filename] = Path(directory) / filename