"""Utilities to create a common interface to all the scripts."""
import argparse
import sys
from loguru import logger
from .log import setup_log
import pkg_resources
from difflib import get_close_matches
import inspect


def run_script(module, prog_name, argc=None):
    """Run script from command line.

    :param add_to_parser: function that takes an argument parser and adds information to it
    :param run_from_args: function that runs the script based on the arguments of the parser
    :param argc: command line arguments
    """
    doc_string = module.__doc__

    logger.enable('mcot')
    script_logger = logger.opt(depth=1)
    if hasattr(module, 'main'):
        setup_log()
        script_logger.info('starting script')
        try:
            module.main(argc)
        except:
            script_logger.exception('Failed script')
    else:
        parser = argparse.ArgumentParser(prog_name, description=doc_string, formatter_class=argparse.RawTextHelpFormatter)

        if inspect.ismodule(module):
            module.add_to_parser(parser)

            args = parser.parse_args(argc[1:])

        setup_log()

        script_logger.info('starting script')
        try:
            if inspect.ismodule(module):
                module.run_from_args(args)
            else:
                module(argc[1:])
        except Exception:
            script_logger.exception('failed script')
            raise
    script_logger.info('finished script')


def run(argv=None):
    plugins = {}
    for ep in pkg_resources.iter_entry_points("mcot"):
        plugins[ep.name] = ep

    if argv is None:
        argv = sys.argv[1:]

    if len(argv) > 0:
        if argv[0] in plugins:
            torun = plugins[argv[0]].load()
            run_script(torun, 'mcot ' + argv[0], argv)
            return
        options = set(
            list(get_close_matches(argv[0], plugins.keys())) + 
            [k for k in plugins if k.startswith(argv[0])]
        )
    else:
        options = []
    print('Usage: mcot [<script_group>...] <script_name> <args>...')
    if len(options) == 0:
        print('Available scripts:')
        for name in sorted(plugins):
            print('- ' + name)
        print('')
        print("Error: Incomplete or invalid script name provided")
    else:
        print("Error: Invalid script name provided. Did you mean one of:")
        for name in sorted(options):
            print('- ' + name)
    exit(1)



