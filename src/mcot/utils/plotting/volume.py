import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from matplotlib.figure import Figure
from typing import Union, Sequence


def plot_slices(arr: np.ndarray, dims: Sequence, flags=None, figure=None, axes=None) -> Union[Figure, FuncAnimation]:
    """
    Create a grid of images or movies from an N-dimensional array

    Each dimension of the array can be expanded into:
    - x-direction of the plot ('x')
    - y-direction of the plot ('y')
    - frame ID of the movie ('time')
    - rows ('row')
    - columns ('col')
    The meaning of each dimension is defined by `dims`.

    :param arr: N-dimensional array with up to 5 dimensions
    :param dims: (N, ) sequence with for each array dimension one of 'x', 'y', 'time', 'row', or 'col' as defined above
    :param flags: dictionary of keyword arguments to pass on `plt.imshow`
        if the flags are a list of lists it is expanded to give a different `flag` 
        to each row (first dimension) and column (second dimension)
    :param figure: matplotlib figure to use for plotting (creates a new one of appropriate size by default)
    :param axes: (nrow, ncol) set of subplots (created if not provided)
    :return: matplotlib `Figure` if there is no "time" dimension, otherwise matplotlib `FuncAnimation`
    """
    if flags is None:
        flags = {}

    if arr.ndim != len(dims):
        raise ValueError("Each dimension in arr should be assigned one of 'x', 'y', 'time', 'row', 'col'")

    def get_flag(value, idx_row, idx_col):
        if not isinstance(value, list):
            return value
        col_value = value[0] if len(value) == 1 else value[idx_row]
        return col_value[0] if len(col_value) == 1 else col_value[idx_col]

    shapes = {dim: sz for dim, sz in zip(dims, arr.shape)}

    if figure is None:
        figure = plt.figure(figsize=(
            shapes.get('col', 1) * shapes.get('x') / 10,
            shapes.get('row', 1) * shapes.get('y') / 10
        ))

    axes = figure.subplots(shapes.get('row', 1), shapes.get('col', 1), squeeze=False)

    def generate_frame(idx_time):
        for idx_row, axes_row in enumerate(axes):
            for idx_col, ax in enumerate(axes_row):
                selector = []
                for dim in dims:
                    if dim == 'row':
                        value = idx_row
                    elif dim == 'col':
                        value = idx_col
                    elif dim == 'time':
                        value = idx_time
                    else:
                        value = slice(None)
                    selector.append(value)
                single_slice = arr[tuple(selector)]
                if dims.index('y') > dims.index('x'):
                    single_slice = single_slice.T
                ax.cla()
                ax.imshow(single_slice, **{name: get_flag(value, idx_row, idx_col) for name, value in flags.items()})
                ax.axis('off')
        figure.set_facecolor('black')
        figure.tight_layout()

    if 'time' in dims:
        anim = FuncAnimation(figure, generate_frame, frames=shapes['time'])
        return anim
    else:
        generate_frame(None)
        return figure