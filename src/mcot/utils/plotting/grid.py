from matplotlib.gridspec import GridSpec, GridSpecFromSubplotSpec


def default_grid(nsubplots, subplot_spec=None, **kwargs) -> GridSpec:
    """
    Creates a default layout for identically sizes subplots

    :param nsubplots: number of subplots
    :param subplot_spec: which subplot the new subplots should be contained in
    :param kwargs: additional parameters defining the spacing of the subplots
    :return: new gridspec
    """
    nrows, ncols = _grid_shape(nsubplots)
    if subplot_spec is None:
        return GridSpec(nrows, ncols, **kwargs)
    else:
        return GridSpecFromSubplotSpec(nrows, ncols, subplot_spec, **kwargs)


def _grid_shape(nsubplots):
    """
    Gets a decent default shape for `nsubplots` sub-plots

    :param nsubplots: number of plots to fit in a grid
    :return: (nrows, ncols) tuple
    """
    if nsubplots <= 3:
        return (1, nsubplots)
    for ncols in range(int(np.floor(np.sqrt(nsubplots))), 1, -1):
        if nsubplots % ncols == 0:
            nrows = nsubplots // ncols
            if nrows <= 3:
                return ncols, nrows
            return nrows, ncols
    return _grid_shape(nsubplots + 1)

