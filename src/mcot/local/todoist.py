from todoist_api_python.api import TodoistAPI
from functools import lru_cache


class LazyTodoistAPI:
    def __init__(self, secret=None):
        if secret is None:
            from .secrets import SECRETS
            secret = SECRETS["todoist_api"]
        self._secret = secret

    @property
    def secret(self, ):
        return self._secret

    @property
    @lru_cache
    def api(self, ):
        return TodoistAPI(self.secret)

    @property
    @lru_cache
    def projects(self, ):
        return self.api.get_projects()

    def find_project(self, name):
        """
        Finds the todoist project with specific name.
        """
        for project in self.projects:
            if project.name == name:
                return project
        raise ValueError(f"No Todoist project found for `{name}`")

    def find_gitlab_project(self, name):
        code_project = self.find_project("Code")
        try:
            project = self.find_project(name)
        except ValueError:
            project = self.api.add_project(name, parent_id=code_project.id)
        assert project.parent_id == code_project.id
        return project
