"""Functions that rely on `choose_gui` to interactively allow user to select from some options."""
from typing import Iterable, Optional
from subprocess import run
import os
from os.path import expanduser


def choose(options: Iterable[str]) -> Optional[str]:
    """
    Asks user to select one of the `options`.

    Requires `choose_gui` to be installed on OSX to work.
    """
    input = '\n'.join(options)
    choice = run(['choose'], capture_output=True, input=input, text=True).stdout
    return None if choice == '' else choice


def datalad_open(argv=None):
    """Ask user to select a project to open in Visual Studio Code."""
    remote = os.environ.get('jal00')

    where = choose(['local', 'remote', 'code', 'julia'])
    if where == 'local':
        ria = os.environ.get("RIA")
        available = run(['ls', expanduser('~/Work/datalad')], capture_output=True, text=True).stdout.splitlines()
    elif where == 'remote':
        available = run(['ssh', remote, "ls ~/Work/datalad"], capture_output=True, text=True, check=True).stdout.splitlines()
        ria = "ria+file:///home/fs0/ndcn0236/scratch/ria-store"
    elif where in ['code', 'julia']:
        directory = {'code': '~/Work/code', 'julia': '~/.julia/dev'}[where]
        available = run(['ls', expanduser(directory)], capture_output=True, text=True).stdout.splitlines()
        to_open = choose(available)
        if to_open is None:
            raise ValueError("User did not select a code project")
        run(['code', expanduser(os.path.join(directory, to_open))])
        return
    else:
        raise ValueError("Invalid choice where to open the project")

    to_open = choose(['<new>'] + available)
    if to_open == '<new>':
        all_projects = run(['ssh', remote, "ls ~/scratch/ria-store/alias"], capture_output=True, text=True, check=True).stdout.splitlines()

        to_checkout = choose(all_projects)
        if to_checkout is not None and to_checkout not in available:
            if where == 'local':
                run(['conda', 'run', '-n', 'datalad', 'datalad', 'install', '-r', '-s', f'{ria}#~{to_checkout}', expanduser(f'~/Work/datalad/{to_checkout}')])
            elif where == 'remote':
                run(['ssh', remote, f'datalad install -r -s {ria}#~{to_checkout} /home/fs0/ndcn0236/Work/datalad/{to_checkout}'])
            else:
                raise ValueError(f'datalad checkout not defined for {where}')
        to_open = to_checkout

    if to_open is None:
        raise ValueError("User did not select a project")
    if where == 'local':
        run(['code', expanduser(f'~/Work/datalad/{to_open}')])
    elif where == 'remote':
        run(['code', '--folder-uri', f'vscode-remote://ssh-remote+{remote}/home/fs0/ndcn0236/Work/datalad/{to_open}'])
    else:
        raise ValueError(f'vscode open not defined for {where}')