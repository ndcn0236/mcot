"""Cleans Obsidian vault"""
from argparse import ArgumentParser
import os.path as op
import os
from glob import glob
from subprocess import run
from mcot.bibtex.file import BibTexFile
from mcot.bibtex.entry import BibTexEntry
import itertools
import re
from unicodedata import normalize
import json
from typing import Collection
from .todoist import LazyTodoistAPI


def get_header(entry: BibTexEntry):
    title = re.sub(r'\{\{(.*?)\}\}', r'\1', entry.tags['title'])
    tag_title = title.replace(':', ';')
    metadata = {"type": entry.type}
    if entry.type == 'book':
        metadata['aliases'] = tag_title
    if 'title' in entry.tags:
        metadata['title'] = tag_title
    if 'author' in entry.tags:
        author_list = re.sub(r"\{\\.*(.)\}", r"\1", entry.tags["author"])
        metadata['author_list'] = f'"{author_list}"'

        metadata['nauthors'] = len(entry.authors)
        metadata['first'] = entry.authors[0].full_name
        metadata['last'] = entry.authors[-1].full_name
    metadata['key'] = entry.key
    if "year" in entry.tags:
        metadata['year'] = entry.tags["year"]
    lines = [f"{key}: {value}" for key, value in metadata.items()]
    lines.insert(0, "---")
    lines.append("---")
    if 'title' in entry.tags:
        lines.append('# ' + title)
    if 'author' in entry.tags:
        def get_name(name):
            prev_name = None
            while prev_name != name.full_name and (len(name.first) <= 1 or (len(name.first) == 2 and name.first[-1] == '.')):
                prev_name = name.full_name
                name.first = ''
                name.full_name = name.full_name
            if name.last.lower() in ('initiative', 'consortium'):
                name.first = {
                    "for the Alzheimer's": "Alzheimer's",
                    "the Alzheimer's": "Alzheimer's",
                }.get(name.first, name.first)
                return name.full_name
            return re.sub(r"\{\\.*(.)\}", r"\1", f"{name.first} {name.last}")
        lines.append(f'- first:: [[people/{get_name(entry.authors[0])}]]')
        if len(entry.authors) > 1:
            lines.append(f'- last:: [[people/{get_name(entry.authors[-1])}]]')
    lines.append(f'- [Zotero link](zotero://select/items/@{entry.key})')
    if 'file' in entry.tags:
        for fn in entry.tags['file'].split(';'):
            if not fn.endswith('.pdf'):
                continue
            index = op.basename(op.dirname(fn))
            filename = op.basename(fn)
            lines.append(f"- [[zotero-pdf/{index}/{filename}|pdf]]")
    lines.append('')
    lines.append('')
    return '\n'.join(lines)


def load_bibtex(vault: str):
    bt = BibTexFile(op.join(vault, 'zotero.bib'))
    return bt


def zotero_metadata(vault: str, update_header: Collection[str], **kwargs):
    """Updates the metadata part of the notes files from zotero"""
    bt = load_bibtex(vault)
    to_remove = []
    changed_headers = []
    for filename in glob(op.join(vault, "*.md")):
        key = op.basename(filename)[:-3]
        key_norm = normalize("NFKD", key)
        if key_norm in bt.entries:
            print(f'{key} in wrong folder, moving to zotero')
            os.rename(filename, op.join(vault, 'zotero', key + '.md'))

    for filename in glob(op.join(vault, "zotero", "*.md")):
        key = op.basename(filename)[:-3]
        key_norm = normalize("NFKD", key)
        if key_norm not in bt.entries:
            to_remove.append(key + '.md')
            print(f"zotero entry for {key} not found")
            continue
    
    for key in bt.entries:
        is_new_note = False
        entry = bt.entries[key]
        filename = op.join(vault, "zotero", key + '.md')
        split = '## Notes\n'
        if op.exists(filename):
            with open(filename, 'r') as f:
                lines = f.readlines()
            if split not in lines:
                print(f"No notes section in {key}, interpreting everything as notes")
                lines.insert(0, '## Notes\n')
                is_new_note = True
        else:
            lines = [split]
            is_new_note = True
        header = get_header(entry)
        idx_split = lines.index(split)
        if header.splitlines() == [l[:-1] for l in lines[:idx_split]]:
            continue

        if not (is_new_note or key in update_header):
            changed_headers.append(key)
            continue

        if is_new_note:
            print(f'New file created for {key}')
        else:
            print(f'updating header of {key}')
        with open(filename, 'w') as f:
            f.write(header)
            f.writelines(lines[idx_split])
            if is_new_note:
                f.write("#process\n")
            f.writelines(lines[idx_split+1:])
    if len(to_remove) > 0:
        print('To remove unrecognised keys run: rm ' + ' '.join(to_remove))
    if len(changed_headers) > 0:
        print("Headers have been updated for following note files: " + " ".join(changed_headers))
        print("To revert changed headers run: mcot clean_obsidian zotero_metadata -u <keys>")


def remove_empty(vault: str, **kwargs):
    """Removes empty files"""
    for filename in glob(op.join(vault, "*.md")):
        if op.getsize(filename) == 0:
            print('removing', op.split(filename)[-1])
            os.remove(filename)


def literature_links(vault: str, **kwargs):
    """Fix literature links
    """
    pattern = re.compile(r'@([-\w]*)')
    bt = load_bibtex(vault)

    def fix_line(line):
        if not '@' in line:
            return line
        while True:
            for match in pattern.finditer(line):
                key = match.group(1)
                if key in bt.entries:
                    line = (
                        line[:match.span()[0]] +
                        '[[' + key + ']]' +
                        line[match.span()[1]:]
                    )
                    break
                else:
                    print(f'unrecognise reference: {key}')
            else:
                break
        return line


    for filename in itertools.chain(
        glob(op.join(vault, "*.md")),
        glob(op.join(vault, "*/*.md"))
    ):
        with open(filename, 'r') as f:
            lines = f.readlines()
        new_lines = [fix_line(l) for l in lines]
        with open(filename, 'w') as f:
            f.writelines(new_lines)


template_gitlab_issue = """---
global_id: {id}
id: {iid}
created: {created}
started: false
done: {done}
closed: {closed}
deadline: 
tags:
    - gitlab
    - issue
    - project
url: {web_url}
---

parent:: [[{project}]]

# Description
{description}

# Notes
"""

def update_gitlab_issues(vault: str, **kwargs):
    glab_cmd = run(["glab", "api", "projects", "--paginate"], capture_output=True)
    if len(glab_cmd.stderr) > 0:
        raise ValueError(f"glab returned with : {glab_cmd.stderr}")
    projects = {}
    for page in glab_cmd.stdout[1:-1].split(b"]["):
        projects.update({p["path_with_namespace"]: p["id"] for p in json.loads(b"[" + page + b"]")})

    todoist = LazyTodoistAPI()

    for directory, _, files in os.walk(vault):
        if directory.startswith("."):
            continue
        for part_file in files:
            file = os.path.join(directory, part_file)
            if not file.endswith(".md"):
                continue
            project_id = None
            read_state = 0
            with open(file, "r") as f:
                for line in f.readlines():
                    if line.strip() == "# Gitlab issues":
                        read_state = 1
                    elif read_state == 1:
                        url = line.strip()
                        path_with_namespace = "/".join(url.split("/")[3:]).lower()
                        project_id = projects[path_with_namespace]
                        read_state = 0
            if project_id is not None:
                print(f"updating gitlab issues in {file}")
                lines = []
                with open(file, "r") as f:
                    reading = True
                    for line in f.readlines():
                        if line.strip() == "# Gitlab issues":
                            lines.append(None)
                            reading = False
                        elif line.startswith("# "):
                            reading = True
                        if reading:
                            lines.append(line)
               
                glab_cmd = run(["glab", "api", f"projects/{project_id}/issues?scope=all", "--paginate"], capture_output=True)
                if len(glab_cmd.stderr) > 0:
                    raise ValueError(f"glab returned with : {glab_cmd.stderr}")
                issues = []
                for page in glab_cmd.stdout[1:-1].split(b"]["):
                    issues.extend([p for p in json.loads(b"[" + page + b"]")])
  
                project_todoist = todoist.find_gitlab_project(path_with_namespace.split("/")[-1])
                tasks = {task.content: task for task in todoist.api.get_tasks(filter=f"#{project_todoist.name}") if task.project_id == project_todoist.id}

                open_issues_dict = {}
                closed_issues_dict = {}
                for issue in issues:
                    assert issue["project_id"] == project_id
                    closed = issue["state"] != "opened"
                    
                    my_creation = issue["author"]["username"] == "ndcn0236"
                    assigned = any(assignee["username"] == "ndcn0236" for assignee in issue["assignees"])

                    symbols = ""
                    if my_creation:
                        symbols = symbols + "📝️"
                    if assigned:
                        symbols = symbols + "🛠️️"
                    symbol = "x" if closed else " "
                    to_store = closed_issues_dict if closed else open_issues_dict
                    to_store[(not (my_creation or assigned), symbol, -issue["id"])] = f"- [{symbol}] [{symbols}{issue['title']}]({issue['web_url']})"

                    if assigned or my_creation:
                        if issue["title"] not in tasks:
                            if not closed:
                                todoist.api.add_task(issue["title"], project_id=project_todoist.id, description=issue["web_url"], order=issue["id"])
                        elif closed:
                            task = tasks[issue["title"]]
                            todoist.api.close_task(task_id=task.id)

                closed_issues_text = '\n'.join([closed_issues_dict[k] for k in sorted(closed_issues_dict.keys())])
                open_issues_text = '\n'.join([open_issues_dict[k] for k in sorted(open_issues_dict.keys())])

                with open(file, "w") as f:
                    for line in lines:
                        if line is None:
                            f.write(f"# Gitlab issues\n{url}\n## Open Gitlab issues\n{open_issues_text}\n## Closed Gitlab issues\n{closed_issues_text}\n")
                        else:
                            f.write(line)



all_commands = [f.__name__ for f in [
    update_gitlab_issues, zotero_metadata, remove_empty, literature_links
]]


default_commands = [f.__name__ for f in [
    zotero_metadata, 
    remove_empty,
    update_gitlab_issues,
]]


def run_from_args(args):
    """Runs the script based on a Namespace containing the command line
    arguments."""
    if not args.command:
        for name in default_commands:
            globals()[name](args.vault, update_header=args.update_header)
    else:
        globals()[args.command](args.vault, update_header=args.update_header)


def add_to_parser(parser: ArgumentParser):
    """Creates the parser of the command line arguments."""
    parser.add_argument("command", choices=list(all_commands), nargs='?', help=f'defaults to running {", ".join(default_commands)}')
    parser.add_argument("-v", "--vault", help="which obsidian vault to edit", default=op.expanduser("~/vault"))
    parser.add_argument("-u", "--update-header", default=(), help="For which zotero keys to update the obsidian note header", nargs="+")
    return parser

