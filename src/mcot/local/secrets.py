import os

def load_secrets(filename=os.path.expanduser("~/.secrets")):
    res = {}
    with open(filename, "r") as f:
        for line in f.readlines():
            key, value = line.split(":", 1)
            res[key] = value.strip()
    return res

try:
    SECRETS = load_secrets()
except FileNotFoundError:
    print("Secrets file not found.")