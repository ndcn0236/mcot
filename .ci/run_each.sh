#!/usr/bin/env sh
set -e

for direc in utils "local" pipe ; do
    cd $direc
    $@
    cd ..
done