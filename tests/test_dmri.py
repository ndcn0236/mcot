from mcot.pipe import dmri
import numpy as np


def test_round_bvals():
    arr = np.array([0, 1000, 5, 995, 1000, 2000, 0, 0, 1000, 2005, 2000])
    assert np.all(dmri.round_bvals(arr) == [0, 1000, 0, 1000, 1000, 2000, 0, 0, 1000, 2000, 2000])