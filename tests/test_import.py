import importlib
from argparse import ArgumentParser
from operator import xor
import pytest
import pkg_resources
import inspect


def get_names(scripts=None):
    return pkg_resources.iter_entry_points("mcot")


@pytest.mark.parametrize('entry_point', get_names())
def test_import(entry_point):
    mod = entry_point.load()

    if inspect.ismodule(mod):
        assert hasattr(mod, 'run_from_args') and hasattr(mod, 'add_to_parser')
